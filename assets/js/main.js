// slick carrosel 

$('.slide').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    arrow: true,
    responsive: [
        {
            breakpoint: 599,
            settings: {
              dots: true 
        }
    }
    ]

    });